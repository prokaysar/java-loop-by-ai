
package loopdemo;

import java.util.Scanner;

public class SumOftwonumberWithWhileLoop {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x,y;
        System.out.print("Enter two number = ");
        x= input.nextInt();
        y= input.nextInt();
        int i=x;
        int j=0;
        while (i<=y) {  
            j=j+i;
            System.out.println("Total = "+j);
            i++;
        }
    }
}
